#!/usr/bin/env python


def convert_to_uid(devices, item):
    if devices.has_key(item):
        return devices[item]


def get_all_devices(data):
    all_devices = dict()
    uid = 0
    for e in data:
      if e['attr']['sysName']:
          all_devices[e['attr']['sysName'][0]] = uid
          uid += 1
      for i in e['attr']['lldpRemSysName']:
          all_devices[i] = uid
          uid += 1
    return all_devices


def get_all_relationships(data, all_devices):
    relationship = list()
    for i in data:
        if i['attr']['sysName']:
            for e in i['attr']['lldpRemSysName']:
                something = i['attr']['sysName'][0]
                relationship.append( { something : convert_to_uid(all_devices, something) ,
                                       e : convert_to_uid(all_devices, e) } )
    return relationship
