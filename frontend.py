#!/usr/bin/env python
from flask import Flask
from flask import request
from flask import render_template
from flask import make_response
from flask import abort
from flask import url_for
from flask import redirect
from flask import jsonify
import requests
import discovery_parser as dp

app = Flask(__name__)


@app.route("/input", methods=['GET', 'POST'])
def input():
    return render_template("input.html")

@app.route("/plot", methods=['POST'])
def plot():
    if request.form.has_key('traffic'):
        neighbors = requests.get('http://10.0.0.137/discovery/device/%s/traffic/neighbors' % request.form['network'])
        source_fqdn = requests.get('http://10.0.0.137/discovery/device/%s/hostname' % request.form['network'])
        
        all_devices = { idx+1 : { 'hostname': i['hostname'], 'traffic': i['traffic'] } for idx, i in enumerate(neighbors.json()) } 
        
        all_devices[0] = { 'hostname' : source_fqdn.json() }
        
        relationship = list()
        
        for i in all_devices:
            if i != 0:
                relationship.append( [0, i, all_devices[i]['traffic']['speed_mbps'], all_devices[i]['traffic']['input_rate_mbps'], all_devices[i]['traffic']['output_rate_mbps'] ] )
        
        response = render_template('traffic.html', relationship=relationship, nodes=all_devices)
    else:        
        api_query = requests.get('http://10.0.0.137/discovery/network/%s/%s' % (request.form['network'], request.form['mask']))

        # check if API returned OK status
        if api_query.status_code != 200:
           abort(404)
        
        # serializes the response to json
        data = api_query.json()
        
        # gets a dictionary with all devices
        all_devices = dp.get_all_devices(data)
        
        # finds out all neighboring relationships
        all_relationships = dp.get_all_relationships(data, all_devices)
        
        response = render_template('plot.html', relationships=all_relationships, node_list=all_devices)
        
    return response

@app.errorhandler(404)
def not_found(error):
    resp = make_response(render_template('404.html'), 404)
#    resp.headers['X-Something'] = 'A value' 
    return resp

@app.errorhandler(405)
def method_not_allowed(error):
    resp = make_response(render_template('405.html'), 405)
#    resp.headers['X-Something'] = 'A value'
    return resp

if __name__ == "__main__":
    app.run(host="0.0.0.0", port="8080")
